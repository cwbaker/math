//
// TestQuat.cpp
// Copyright (c) 2010 Charles Baker.  All rights reserved
//

#include "stdafx.hpp"
#include <sweet/unit/UnitTest.h>
#include <sweet/math/quat.hpp>

using namespace sweet::math;

SUITE( TestQuat )
{
    TEST( TestMultiplication )
    {
    }    

    TEST( TestAddition )
    {
    }    

    TEST( TestScalarMultiplication )
    {
    }    

    TEST( TestDot )
    {
    }
    
    TEST( TestConjugate )
    {
    }
       
    TEST( TestNormalize )
    {
    }    

    TEST( TestSlerp )
    {
    }
}
