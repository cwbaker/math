//
// TestVec2.cpp
// Copyright (c) 2010 Charles Baker.  All rights reserved
//

#include "stdafx.hpp"
#include <sweet/unit/UnitTest.h>
#include <sweet/math/vec2.hpp>

using namespace sweet::math;

SUITE( TestVec2 )
{
    TEST( TestAddition )
    {
    }    

    TEST( TestSubtraction )
    {
    }    

    TEST( TestMultiplication )
    {
    }    

    TEST( TestScalarMultiplication )
    {
    }    

    TEST( TestLength )
    {
    }    

    TEST( TestNormalize )
    {
    }    

    TEST( TestLerp )
    {
    }
}
